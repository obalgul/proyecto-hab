const { getConnection } = require("../../db");

async function listUser(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { id } = req.params;

    //Hacemos la consulta de los datos del usuario
    const [result] = await connection.query(
      `SELECT * 
       FROM users
       WHERE id=?
      `,
      [id]
    );

    console.log(result);
    //devolvemos la consulta
    res.send({
      status: "ok",
      data: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = listUser;
