const { getConnection } = require("../../db");
const { randomString, sendMail, generateError } = require("../../helpers");

const {
  recoveryPasswordUserSchema,
} = require("../../validators/userValidators");

async function recoveryPasswordUser(req, res, next) {
  let connection;

  try {
    //Validamos el mail
    await recoveryPasswordUserSchema.validateAsync(req.body);

    connection = await getConnection();

    const { email } = req.body;

    //comprobamos que existe el usuario
    const [current] = await connection.query(
      `
        SELECT id
        FROM users
        WHERE email=?
        `,
      [email]
    );

    if (current.length === 0) {
      throw generateError(
        `Opsss, tenemos un problema, no tenemos ninguna dirección de correo ${email} registrado en nuestra base de datos.`,
        404
      );
    }

    //añadimos un código aleatorio al usuario en la base de datos
    const recoveryCode = randomString(40);
    console.log(recoveryCode);
    await connection.query(
      `
        UPDATE users
        SET recovery_code=?
        WHERE email=?
        `,
      [recoveryCode, email]
    );

    //envio del código por correo
    try {
      await sendMail({
        email: email,
        title: "Código para resetear la contraseña SHOPTEC.",
        content: `Has solicitado la recuperación de tu contraseña. 
                  Este es tu código de recuperación:
                  ${recoveryCode}.
                  Si no has solicitado la recuperación de la contraseña,
                  no hagas caso a este correo. No se realizaran cambios
                  en tu cuenta.
                  `,
      });
    } catch (error) {
      throw generateError(
        "Opsss, hmos tenido un problema al enviar el correo.",
        500
      );
    }

    //respondemos al usuario
    res.send({
      status: "ok",
      message:
        "Ha sido enviado un correo a la cuenta asociada al usuario, con instrucciones para recuperar la contraseña.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = recoveryPasswordUser;
