const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

async function validateUser(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    //desestructuramos datos de code
    const { code } = req.params;

    //consultamos la bbdd con el datp de code si no corresponde devolvemos un error
    const [result] = await connection.query(
      `
      SELECT email
      FROM users
      WHERE registration_code=?
    `,
      [code]
    );

    if (result.length === 0) {
      throw generateError(
        "Vaya! No encontramos a ningún usuario pendiente de validación con este código",
        404
      );
    }

    // Actualizar la tabla de usuarios marcando como activo
    // el usuario que tenga el código de registro recibido
    await connection.query(
      `
      UPDATE users
      SET active=1, registration_code=NULL
      WHERE registration_code=?
    `,
      [code]
    );

    //enviamos una respuesta
    res.send({
      status: "ok",
      message: `Ya puedes hacer login con tu email: ${result[0].email} y tu contraseña`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = validateUser;
