const { getConnection } = require("../../db");

async function listUserArticle(req, res, next) {
  let connection;

  try {
    connection = await getConnection();

    const { id } = req.params;

    //Hacemos la consulta de los datos del usuario
    const [result] = await connection.query(
      `SELECT * 
       FROM articles
       WHERE id_seller=?
      `,
      [id]
    );

    console.log(req.params);
    //devolvemos la consulta
    res.send({
      status: "ok",
      data: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = listUserArticle;
