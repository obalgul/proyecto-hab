const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

const { voteArticleSchema } = require("../../validators/articlesValidators");

async function voteArticle(req, res, next) {
  let connection;

  try {
    //Ponemos el precio para que solo permita números
    req.body.vote = Number(req.body.vote);

    //validamos los datos a cambiar
    await voteArticleSchema.validateAsync(req.body);

    connection = await getConnection();

    //Sacamos el id del artículo
    const { id } = req.params;
    const { vote } = req.body;

    //obtenemos los datos para poder realizar el voto del
    //compradr y del vendedor
    const [voteData] = await connection.query(
      `
    SELECT confirmed, id_seller, id_buyer
    FROM reservation
    WHERE id_article=?
    `,
      [id]
    );

    const [resultVote] = voteData;

    //comprobamos que el artículo tiene confirmada su reserva.
    if (resultVote.confirmed !== 1) {
      throw generateError(
        "Upsss, este artículo no se encuentra en estado reservado, no se puede realizar el voto",
        403
      );
    }

    //Guardamos el valor del voto en el campo rating
    //de la tabla reservation.
    await connection.query(
      `
    UPDATE reservation
    SET rating=?, update_date=UTC_TIMESTAMP
    WHERE id_article=?
    `,
      [vote, id]
    );

    ///sacamos de la base de datos los datos de la tabla artículos
    const [dataArticle] = await connection.query(
      `
    SELECT brand, description
    FROM articles
    WHERE id=?
    `,
      [id]
    );

    const [currentData] = dataArticle;

    res.send({
      status: "Ok",
      message: `Se ha computado correctamente tu Voto de ${vote} puntos, al artículo marca ${currentData.brand}, ${currentData.description}`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = voteArticle;
