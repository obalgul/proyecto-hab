const { getConnection } = require("../../db");
const { processAndSaveImage, generateError } = require("../../helpers");

const {
  updateImageArticleSchema,
} = require("../../validators/articlesValidators");

async function updateArticleImage(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    //cogemos los datos del artículo
    const { id } = req.params;

    const { slot } = req.body;
    console.log(req.body);
    //Comprobar co joi que slot exista y tenga un valor 1, 2 o 3
    //validamos los datos a cambiar
    await updateImageArticleSchema.validateAsync(req.body);

    // Comprobar que el usuario puede actualizar las imagenes del artículo
    // Seleccionar el artículo por su id
    const [current] = await connection.query(
      `
        SELECT id_seller
        FROM articles
        WHERE id=?
      `,
      [id]
    );

    // Comprobamos que el usuario puede editar el artículo
    const [currentEntry] = current;

    if (currentEntry.id_user === req.auth.id && req.auth.role !== 1) {
      throw generateError(
        "Opsss. Lo sentimos pero no tienes permisos para editar este artículo.",
        403
      );
    }

    // Subir la imagen
    if (req.files && req.files.image) {
      // Procesamos e insertamos imagen
      try {
        const processedImage = await processAndSaveImage(req.files.image);
        await connection.query(
          `
          UPDATE articles SET image${slot}=?
          WHERE id=?
        
          `,
          [processedImage, id]
        );
        console.log(processedImage);
      } catch (error) {
        console.error(error);
        throw generateError(
          "Ooooh, vaya. No hemos podido procesar la imagen. Inténtalo de nuevo",
          400
        );
      }
    } else {
      throw generateError("Upss, la imagen no se ha subido", 400);
    }

    res.send({
      status: "ok",
      message: "Imagen subida",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = updateArticleImage;
