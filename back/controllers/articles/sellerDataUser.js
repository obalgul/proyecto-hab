const { getConnection } = require("../../db");
const { generateError } = require("../../helpers");

async function sellerDataUser(req, res, next) {
  let connection;

  try {
    connection = await getConnection();
    // desestructuramos el id
    const { id } = req.params;
    console.log(req.params);
    // Sacamos los datos de los artículos reservados y prereservados para mostrar en el hitorial de ventas
    const [result] = await connection.query(
      `
      SELECT R.reservation_date, R.reservation_code, A.category, 
	        A.brand, A.description, A.price, A.state, A.image1, A.image2, A.image3,  R.rating, U.name, U.last_name
      FROM articles A INNER JOIN reservation R ON A.id=R.id_article
	        LEFT OUTER JOIN users U ON U.id = R.id_buyer
      WHERE R.id_seller=?
      GROUP BY R.id
      ORDER BY R.id_seller ASC
      `,
      [id]
    );

    //comprobamos que solo el usuario con su id y el admin accedan
    if (req.auth.id !== Number(id) && req.auth.role !== 1) {
      throw generateError(
        `Upsss. Hemos tenido un problema. O el usuario no existe o no tienes permiso de acceso a este sitio.`,
        404
      );
    }

    //devolvemos una respuesta
    res.send({
      status: "ok",
      data: result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = sellerDataUser;
