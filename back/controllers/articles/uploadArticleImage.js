const { getConnection } = require("../../db");
const { processAndSaveImage, generateError } = require("../../helpers");

async function uploadArticleImage(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    //cogemos los datos del artículo
    const { id } = req.params;

    // Comprobar que el usuario puede actualizar las imagenes del artículo
    // Seleccionar el artículo por su id
    const [current] = await connection.query(
      `
        SELECT id_seller
        FROM articles
        WHERE id=?
      `,
      [id]
    );

    // Comprobamos que el usuario puede editar el artículo
    const [currentEntry] = current;

    if (currentEntry.id_user === req.auth.id && req.auth.role !== 1) {
      throw generateError(
        "Opsss. Lo sentimos pero no tienes permisos para editar este artículo.",
        403
      );
    }

    // Comprobamos que el artículo tiene por lo menos de 3 imágenes ya asociadas
    const [images] = await connection.query(
      `
      SELECT id
      FROM articles_images
      WHERE id_article=?
    `,
      [id]
    );

    if (images.length >= 3) {
      throw generateError(
        "Upss, Hemos sufrido un problema, no se ha podido subir las imagenes a este artículo, borre alguna imagen primero",
        406
      );
    }

    // Subir la imagen
    if (req.files && req.files.image) {
      // Procesamos e insertamos imagen
      try {
        const processedImage = await processAndSaveImage(req.files.image);
        await connection.query(
          `
          INSERT INTO articles_images (image, save_image_date, id_article)
          VALUES(?, UTC_TIMESTAMP, ?)
          `,
          [processedImage, id]
        );
      } catch (error) {
        throw generateError(
          "Ooooh, vaya. No hemos podido procesar la imagen. Inténtalo de nuevo",
          400
        );
      }
    } else {
      throw generateError("Upss, la imagen no se ha subido", 400);
    }

    res.send({
      status: "ok",
      message: "Imagen subida",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) connection.release();
  }
}

module.exports = uploadArticleImage;
