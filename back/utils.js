//Trabajar con el TOKEN

import jwt from "jwt-decode";
import axios from "axios";

const ENDPOINT = "http://localhost:3000";

function login(email, password) {
  try {
    axios
      .post(`${ENDPOINT}/auth`, {
        email: email,
        password: password,
      })
      .then(function (response) {
        console.log(response);
        //Guardo el Token
        setAuthToken(response.data.token);
        //guardo el role
        setIsAdmin(response.data.admin);
        //guardo nombre usuario
        setName(response.data.user);
      });
  } catch (error) {
    console.log(error);
  }
}

//Funcion para guardar el localstroage el jsonwebtoken
function setAuthToken(token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  localStorage.setItem("AUTH_TOKEN_KEY", token);
}

//Funcion para recuperar el token
function getAuthToken() {
  return localStorage.getItem("AUTH_TOKEN_KEY");
}

//Funcion para conseguir fecha caducidad del token
function tokenExpiration(encodedToken) {
  let token = jwt(encodedToken);
  if (!token.exp) {
    return null;
  }
  let date = new Date(0);
  date.setUTCSeconds(token.exp);
  return date;
}

//Funcion que comprueba si el token esta pocho
function isExpired(token) {
  let expirationDate = tokenExpiration(token);
  return expirationDate < new Date();
}

//Fucion que comprueba si etta logeada y su token es correcto
function isLoggedIn() {
  let authToken = getAuthToken();
  return !!authToken && !isExpired(authToken);
}

//funcion guardar admin en el localstorage
function setIsAdmin(admin) {
  localStorage.setItem("ROLE", admin);
}

//recuperar admn  del localstorage
function getIsAdmin() {
  return localStorage.getItem("ROLE");
}

//Comprobar que es admin true
function checkIsAdmin() {
  let role = null;
  let admin = getIsAdmin();

  if (admin === "true") {
    role = true;
  } else {
    role = false;
  }
  return role;
}
//funcion de guardar nombre de usurio en le localstorage
function setName(user) {
  localStorage.setItem("NAME", user);
}

//recuperamos el nombre de usuario de localstorage
function getName() {
  return localStorage.getItem("NAME");
}

//Funcion de logout
function logout() {
  axios.defaults.headers.common["Authorization"] = "";
  localStorage.removeItem("AUTH_TOKEN_KEY");
  localStorage.removeItem("ROLE");
  localStorage.removeItem("NAME");
}

module.exports = {
  login,
  setAuthToken,
  getAuthToken,
  tokenExpiration,
  isExpired,
  isLoggedIn,
  setIsAdmin,
  getIsAdmin,
  checkIsAdmin,
  setName,
  getName,
  logout,
};
