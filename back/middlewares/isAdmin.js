//-----------------------------Importamos función del archivo helpers--------------
const { generateError } = require("../helpers");

//creamos una función que comprueba si el usuario es administrador
async function isAdmin(req, res, next) {
  //comprobamos si es admin, si no lo es devolvemos un error
  if (req.auth.role === 1) {
    next();
  } else {
    throw generateError(
      "Uyuyui!, Va a ser que no tienes privilegios de administración",
      403
    );
  }
}

//----------------------exportamos el módulo----------------------
module.exports = isAdmin;
