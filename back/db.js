//----------requerimos el modulo dotenv, para guardar variables de entorno------------
require("dotenv").config();

//-------------------------importamos módulos de npm------------------------------
const mysql = require("mysql2/promise");

//------------------------desestructuramos las variables de entrno-------------------
//--------------------------------que están guardadas en el .env-----------------------
const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

//-----------------------------creamos las conexiones a la base de datos----------------
let pool;

async function getConnection() {
  if (!pool) {
    pool = mysql.createPool({
      connectionLimit: 10,
      host: MYSQL_HOST,
      user: MYSQL_USER,
      password: MYSQL_PASSWORD,
      database: MYSQL_DATABASE,
      timezone: "Z",
    });
  }

  return await pool.getConnection();
}

//--------------------------exportamos la función de conexión---------------------
module.exports = {
  getConnection,
};
