import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

//Importamos la función para poder entrar o no
import { isLoggedIn } from "../utils/utils.js";
//import { checkIsAdmin } from "../utils/utils.js";

Vue.use(VueRouter);

const routes = [
  {
    path: "/home",
    name: "Home",
    component: Home,
    meta: {
      allowUser: true,
    },
  },
  {
    path: "/registred",
    name: "Registro",

    component: () => import("../views/RegistredPage.vue"),
    meta: {
      allowUser: true,
    },
  },
  {
    path: "/validate/activate",
    name: "Activate",

    component: () => import("../views/Activate.vue"),
    meta: {
      allowUser: true,
    },
  },
  {
    path: "/",
    name: "Login",

    component: () => import("../views/LoginPage.vue"),
    meta: {
      allowUser: true,
    },
  },
  {
    path: "/homeuser",
    name: "HomeUser",
    component: () => import("../views/HomeUser.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/listbuy",
    name: "ListBuy",

    component: () => import("../views/ListBuy.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/listseller",
    name: "ListSeller",

    component: () => import("../views/ListSeller.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/confirmreserve",
    name: "ConfirmReserve",

    component: () => import("../views/ConfirmReserve.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/sobremi",
    name: "SobreMi",

    component: () => import("../views/SobreMi.vue"),

    meta: {
      allowUser: true,
    },
  },
  {
    path: "/vote",
    name: "Vote",

    component: () => import("../views/Vote.vue"),
    meta: {
      allowUser: true,
    },
  },
  {
    path: "*",
    name: "Error",
    component: () => import("../views/Error.vue"),
    meta: {
      allowUser: true,
    },
  },
  {
    path: "/newarticle",
    name: "NewArticle",

    component: () => import("../views/NewArticle.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/editArticle",
    name: "EditArticle",

    component: () => import("../views/EditArticle.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/editUser",
    name: "EditUser",

    component: () => import("../views/EditUser.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/resetpassword",
    name: "ResetPassword",

    component: () => import("../views/ResetPassword.vue"),

    meta: {
      allowUser: false,
    },
  },
  {
    path: "/recoveryPassword",
    name: "RecoveryPassword",

    component: () => import("../views/RecoveryPassword.vue"),
    meta: {
      allowUser: true,
    },
  },
  {
    path: "/coderesetpassword",
    name: "CodeResetPassword",

    component: () => import("../views/CodeResetPassword.vue"),

    meta: {
      allowUser: true,
    },
  },
];

const router = new VueRouter({
  routes,
});

//comprobación si es valido el ususario para ver la pagina
router.beforeEach((to, from, next) => {
  if (!to.meta.allowUser && !isLoggedIn()) {
    next({
      path: "/",
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
